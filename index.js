'use strict';

const fs = require('fs');
const readline = require('readline');
const {google} = require('googleapis');
const calendar = google.calendar("v3");
const moment = require('moment');

exports.http = async (request, response) => {
	// if (request.method !== 'GET') {
	// 	response.status(403).send('Forbidden!');
	// }
	console.log(request);

	response.set('Access-Control-Allow-Origin', '*');

	var paramCal = request.query.calendar ? parseInt(request.query.calendar) :  1;

	const calendar = google.calendar({version: 'v3'});
	const authClient = await google.auth.getClient({
		scopes: ['https://www.googleapis.com/auth/calendar',
		         'https://www.googleapis.com/auth/calendar.events'],
	});
	authClient.subject = process.env.AUTHSUBJECT;
	
	var minTime = new Date();
	var maxTime = new Date(minTime);
	maxTime.setHours(23,59,59,999);
	var calId = process.env['CAL' + paramCal];
	const res = await calendar.events.list({
			auth: authClient,
			calendarId:calId, 
			timeMin: minTime.toISOString(),
			timeMax: maxTime.toISOString(),
			maxResults: 2,
			singleEvents: true,
			orderBy: 'startTime',
		});
		
	let currentEvent = null;
	let nextEvent = null;
	if (res && res.data) {
		const events = res.data.items;
		// const nowTime = moment("2020-01-28T10:13:25+00:00");
		const nowTime = moment();
		if (events.length) {
			events.map((event, i) => {
				const start = event.start.dateTime || event.start.date;
				const end = event.end.dateTime || event.end.date;

				const startDate = moment(start);
				const endDate = moment(end);

				// TODO any other useful info, status, creator, organizer
				const eventDetails = {'start': startDate.format('LT'), 
					'end': endDate.format('LT'), 
					'summary': event.summary,
					'creator': event.creator.email
				};

				if (currentEvent == null && nowTime.isBetween(startDate, endDate)) {
					currentEvent = eventDetails;
				}
				else if (nextEvent == null && startDate.isAfter(nowTime)) {
					nextEvent = eventDetails;
				}

			});
		} else {
			console.log('No upcoming events found.');
		}
	}
	else 
		console.log('No res or no res.data');

	var wifi = {'ssid': process.env.WIFI_SSID, 'password': process.env.WIFI_PWD};

	var data = {'current': currentEvent, 'next': nextEvent, 'wifi': wifi};
	response.status(200).json(data);
};

exports.event = (event, callback) => {
  callback();
};
