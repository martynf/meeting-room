# meeting-room

Serverless Node.js Google Cloud project to view/control meeting room availability.

Authorising the service account to ready G Suite data
https://developers.google.com/identity/protocols/OAuth2ServiceAccount?authuser=1

View logs:
https://console.cloud.google.com/logs?authuser=1&project=meeting-room-255011&supportedpurview=project