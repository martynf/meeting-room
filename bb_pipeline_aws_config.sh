#!/bin/bash

mkdir -p ~/.aws

echo "[default]" > ~/.aws/config
echo "output = json" >> ~/.aws/config
echo "region = ${AWS_DEFAULT_REGION}" >> ~/.aws/config
echo "aws_access_key_id = ${AWS_ACCESS_KEY_ID}" >> ~/.aws/config
echo "aws_secret_access_key = ${AWS_SECRET_ACCESS_KEY}" >> ~/.aws/config
echo "[profile rc-resources]" >> ~/.aws/config
echo "source_profile = default" >> ~/.aws/config
echo "region = ${AWS_DEFAULT_REGION}" >> ~/.aws/config
echo "role_arn = ${ROLE_ARN}" >> ~/.aws/config
